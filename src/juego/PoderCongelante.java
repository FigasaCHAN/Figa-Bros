package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class PoderCongelante {
	private double x,y,alto,ancho,angulo,escala;
	private int direccionDelMago;
	private int jugador; //puede valer 1 si es el jugador1; 2 si es el jugador2
	
	//IMAGENES
	private static Image imagen= Herramientas.cargarImagen("poderDerecha.gif");
	private static Image poderDerecha= Herramientas.cargarImagen("poderDerecha.gif");
	private static Image poderIzquierda= Herramientas.cargarImagen("poderIzquierda.gif");
	
	public PoderCongelante(Mago mago, int jugador) {
		this.x=mago.getX();
		this.y=mago.getY();
		this.alto=10;
		this.ancho=10;
		this.angulo=0;
		this.escala=1;
		this.direccionDelMago=mago.getDireccion();  //Siempre empieza mirando a la izquierda.
		this.jugador=jugador;
	}
	
	public void dibujar(Entorno entorno) {
		//Dibujo un rectangulo.
		entorno.dibujarImagen(imagen, x, y, angulo, escala);
	}
	
	public void avanzar() {
		//Detecta la direccion del mago y asi se puede dirigir para un lado o otro.
		if (direccionDelMago==1) {
			imagen= poderDerecha;
			x= x+5;
		}else {
			imagen= poderIzquierda;
			x= x-5;
		}
	}

	public boolean chocasteCon(Escenario escenario) {
		return x+ancho>=escenario.getbarraDerecha().getX()-escenario.getbarraDerecha().getAncho()/2 ||
				x-ancho<=escenario.getbarraIzquierda().getX()+escenario.getbarraIzquierda().getAncho()/2;
	}
	
	//GET Y SET
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public double getJugador() {
		return jugador;
	}
}
