package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Menu {
	private Viga opcion1,opcion2,opcion3;
	private static Image gameOver= Herramientas.cargarImagen("gameOverLogo.gif");
	
	public Menu() {
		this.opcion1= new Viga(390,150,30,300,Color.BLUE);
		this.opcion2= new Viga(390,200,30,300,Color.WHITE);
		this.opcion3= new Viga(390,250,30,300,Color.WHITE);
	}
	
	public void dibujarGameOver(Entorno entorno) {
		entorno.dibujarImagen(gameOver, 405, 250, 0);
	}
	public void dibujarMenuDeInicio(Entorno entorno) {
		opcion1.dibujarViga(entorno);
		opcion2.dibujarViga(entorno);
		opcion3.dibujarViga(entorno);
	}
	public void mostrarOpciones(Entorno entorno) {
		//Se "escribe" el contenido de los "botones".
		entorno.cambiarFont("consola", 20, Color.RED);
		entorno.escribirTexto("New Game", 340, 157);
		entorno.escribirTexto("Multijugador", 340, 207);
		entorno.escribirTexto("Exit", 340, 257);
	}
	public void alternarOpcionesHaciaAbajo(int seleccion) {
		if (seleccion==0) {
			opcion1.setColor(Color.BLUE);
		}
		if (seleccion==1) {
			opcion1.setColor(Color.WHITE);
			opcion2.setColor(Color.BLUE);
		}
		if(seleccion==2) {
			opcion2.setColor(Color.WHITE);
			opcion3.setColor(Color.BLUE);
		}
	}
	public void alternarOpcionesHaciaArriba(int seleccion) {
		if (seleccion==0) {
			opcion1.setColor(Color.BLUE);
			opcion2.setColor(Color.WHITE);
		}
		if (seleccion==1) {
			opcion2.setColor(Color.BLUE);
			opcion3.setColor(Color.WHITE);
		}
		if (seleccion==2) {
			opcion3.setColor(Color.blue);
		}
	}
}
