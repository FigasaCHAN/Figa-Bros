package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;


public class Mago {
	private double x,auxiliarX;
	private double y,auxiliarY;
	private double alto;
	private double ancho;
	private int vidas;
	private int puntos;
	private int direccionDelMago;
	private double posicionDisparo;
	private boolean estaMuerto;
	private PocionVoladora itemPocionVoladora;
	private PocionPoderControlado itemPR;
	private int ID;  //DEVUELVE CUAL MAGO ES. EL 1 O EL 2.

	//IMAGENES
		//PLAYER 1
	private static Image mago0Imagen= Herramientas.cargarImagen("heroeIzquierdaEstatico.png");
	private static Image mago0ImagenEstaticoIzquierda= Herramientas.cargarImagen("heroeIzquierdaEstatico.png");
	private static Image mago0ImagenEstaticoDerecha = Herramientas.cargarImagen("heroeDerechaEstatico.png");
	private static Image mago0ImagenDerecha = Herramientas.cargarImagen("heroeDerecha.gif");
	private static Image mago0ImagenIzquierda= Herramientas.cargarImagen("heroeIzquierda.gif");
		//PLAYER 2
	private static Image mago1Imagen= Herramientas.cargarImagen("heroeIzquierda1Estatico.png");
	private static Image mago1ImagenEstaticoIzquierda= Herramientas.cargarImagen("heroeIzquierda1Estatico.png");
	private static Image mago1ImagenEstaticoDerecha = Herramientas.cargarImagen("heroeDerecha1Estatico.png");
	private static Image mago1ImagenDerecha = Herramientas.cargarImagen("heroeDerecha1.gif");
	private static Image mago1ImagenIzquierda= Herramientas.cargarImagen("heroeIzquierda1.gif");
	
	
	public Mago(double x, double y, double alto, double ancho, int mago) {
		this.x= x;
		this.y= y;
		this.auxiliarX=x;
		this.auxiliarY=y;
		this.alto= alto;
		this.ancho= ancho;
		this.vidas=3;
		this.puntos=0;
		this.direccionDelMago=0;
		this.posicionDisparo=0;
		this.itemPocionVoladora= new PocionVoladora(); //INICIO ESCOBA, INICIA EN FALSE PORQUE NO TIENE LA ESCOBA
		this.itemPR= new PocionPoderControlado();//INICIO PODER RAFAGA, INICIA EN FALSE PORQUE NO TIENE EL ITEM
		this.estaMuerto= false;
		this.ID=mago;
	}
	
	//-------FUNCIONES BASICAS----
	public void dibujarMago0(Entorno entorno) {
		entorno.dibujarImagen(mago0Imagen, x, y, 0,1);
	}
	public void dibujarMago1(Entorno entorno) {
		entorno.dibujarImagen(mago1Imagen, x, y, 0,1);	
	}
	public void moverIzquierda(int mago) {
		if (mago==0) {
			mago0Imagen = mago0ImagenIzquierda;
			direccionDelMago=0;
			x=x-2;
		}else {
			mago1Imagen = mago1ImagenIzquierda;
			direccionDelMago=0;
			x=x-2;
		}
	}
	public void moverDerecha(int mago) {
		if (mago==0) {
			mago0Imagen = mago0ImagenDerecha;
			direccionDelMago=1;
			x=x+2;
		}else {
			mago1Imagen = mago1ImagenDerecha;
			direccionDelMago=1;
			x=x+2;
		}
	}
	public void detenerseMago0(int direccion){
		if (direccion==0) {
			mago0Imagen= mago0ImagenEstaticoIzquierda;
		}else {
			mago0Imagen= mago0ImagenEstaticoDerecha;
		}	
	}
	public void detenerseMago1(int direccion) {
		if (direccion==0) {
			mago1Imagen= mago1ImagenEstaticoIzquierda;
		}else {
			mago1Imagen= mago1ImagenEstaticoDerecha;
		}
	}
	public void caer(){
		y=y+1;
	}
	public void volverAInicio() {
		x=auxiliarX;
		y=auxiliarY;
	}
	
	//-------GET Y SET BASICOS -----
	public int getDireccion() { //Retorna la direccion a la cual esta mirando el mago.
		return direccionDelMago;
	}

	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public boolean estaMuerto() {
		return estaMuerto;
	}
	public int getVidas() {
		return vidas;
	}
	public int getID() {
		return ID;
	}

	//----LIMITES DE MOVIMIENTOS----
	public boolean chocaConBarraIzquierda(Escenario e) {
		return x+ancho/2>=e.getbarraDerecha().getX()-e.getbarraDerecha().getAncho()/2;
	}
	public boolean chocaConBarraDerecha(Escenario e) {
		return x-ancho/2<=e.getbarraIzquierda().getX()+e.getbarraIzquierda().getAncho()/2;
	}
	
	public boolean llegasteAlFinal(Escenario escenario) {
		return y+alto/2>=escenario.agujero.getY()-escenario.agujero.getAlto()/2;
	}
	
	//----INTERACCIONES CON EL HUB----
	//Funciones auxiliares que manejan la vida y los puntos.
	public int cantidadDeVidas() {
		return vidas;
	}
	public void sumarPuntos() {
		puntos=puntos+10;
	}
	public int cantidadDePuntos() {
		return puntos;
	}
	
	public void volverAlInicioSinUnaVida() {
		vidas=vidas-1;
		x=auxiliarX;
		y=auxiliarY;
		itemPocionVoladora.setActivado(false);//significa que murio y cuando lo hace le quito el item de escoba
		itemPR.setActivado(false);
		if (vidas<=0) {
			estaMuerto=true;
			x=auxiliarX;
			y=auxiliarY;
		}
	}
	
	public boolean agujero(Viga viga) {
		if ((y+alto/2>=viga.getY()-viga.getAlto()/2) && (y+alto/2<=viga.getY()+viga.getAlto()/2) 
				&& (viga.getX()-viga.getAncho()/2<= x+ancho/2 
				&& viga.getX()+viga.getAncho()/2>=x-ancho/2)){
			return false; //devuelve false cuando no hay agujero, es decir, que el objeto mago, choca con una viga
		}
		return true; //devuelve true cuando hay agujero
	}
	
	
	//-----Disparo-------
	public void guardarPosicion() {
		posicionDisparo=y;
	}
	public double posicionDisparo() {
		return posicionDisparo;
	}
	public void resetPosicionDisparo() {
		posicionDisparo=0;
	}

	//----------ITEMS---
	//-----ESCOBA-----
	public boolean magoChocaConPocionVoladora(PocionVoladora escoba) {//EL MAGO CHOCA CON LA ESCOBA
		return  x-ancho/2 <= escoba.getX()-escoba.getAncho()/2 &&
				x+ancho/2>= escoba.getX()+escoba.getAncho()/2 &&
				y-alto/2 <= escoba.getY()-escoba.getAlto()/2 &&
				y+alto/2>= escoba.getY()+escoba.getAlto()/2 ;
	}
	public void activarPocionVoladora() {//EL MAGO ACTIVA LA ESCOBA
		itemPocionVoladora.setActivado(true);
	}
	public void desactivarPocionVoladora() {
		itemPocionVoladora.setActivado(false);
	}
	public boolean magoTienePocionVoladora() {
		return itemPocionVoladora.getActivado();//devuelve false cuando no esta activado,  true cuando si
	}
	public boolean chocasteConViga(Viga viga) { 

		if(direccionDelMago==0) {//El mago viene desde la derecha, se esta moviendo a la izquierda
			if(x-ancho/2<= viga.getX()+viga.getAncho()/2 &&
					x+ancho/2 >= viga.getX()+viga.getAncho()/2+ ancho/2 &&
					y-alto/2<= viga.getY()-viga.getAlto()/2 &&
					y+alto/2>= viga.getY() + viga.getAlto()/2
					) {
				return true;}
		}
		if(direccionDelMago==1) {//El mago viene desde la izquierda, se esta moviendo a la derecha
			if(x+ancho/2<= viga.getX()-viga.getAncho()/2 &&
					x+ancho/2 >= viga.getX()-viga.getAncho()/2- ancho/2 &&
					y-alto/2<= viga.getY()-viga.getAlto()/2 &&
					y+alto/2>= viga.getY() + viga.getAlto()/2
					) {
				return true;
			} 
		}
		return false;
	}
	
	//------PODER RAFAGA----
	public boolean magoChocaConPocionPoderControlado(PocionPoderControlado pr) {//EL MAGO CHOCA CON PODER RAFAGA
		return  x-ancho/2 <= pr.getX()-pr.getAncho()/2 &&
				x+ancho/2>= pr.getX()+pr.getAncho()/2 &&
				y-alto/2 <= pr.getY()-pr.getAlto()/2 &&
				y+alto/2>= pr.getY()+pr.getAlto()/2 ;
	}
	public void activarPocionPoderControlado() {//EL MAGO ACTIVA LA ESCOBA
		itemPR.setActivado(true);
	}
	public void desactivarPoderRafaga() {
		itemPR.setActivado(false);
	}
	public boolean magoTienePocionPoderControlado() {
		return itemPR.getActivado();//devuelve false cuando no esta activado,  true cuando si
	}
	
	
	//------COMPRA DE ITEMS---
	public void magoCompraPocionVoladora() {
		if(puntos>=60) {
			activarPocionVoladora();
			puntos-=60;
		}
		else {
			System.out.println("no podes comprar Escoba");
		}
	}
	public void magoCompraPocionPoderControlado() {
		if(puntos>=80) {
			activarPocionPoderControlado();
			puntos-=80;
		}
		else {
			System.out.println("no podes comprar Rafaga");
		}

	}
	public void magoRevive() {
		if(puntos>=20) {//le pongo 20 para probar
			estaMuerto=false;
			if(vidas<=0) { //para evitar que compre vidas
				vidas=3;
			}
			puntos-=20;
		}
		else {
			System.out.println("no podes Revivir");
		}
	}
	
	public void reiniciar() {
		vidas=0;
		estaMuerto=true;
		x=auxiliarX;
		y=auxiliarY;
		return;
	}
	

}