package juego;

import javax.sound.sampled.Clip;

import entorno.Herramientas;

public class Sonidos {
	private static Clip hielo,herirMago,disparo,gameover,revivir;
	private static Clip menu= Herramientas.cargarSonido("Urss.wav");
	
	//Juego
	public static void romperHielo() {
		hielo=Herramientas.cargarSonido("hieloRompiendose.wav");
		hielo.start();
	}
	public static void disparo() {
		disparo= Herramientas.cargarSonido("disparo.wav");
		disparo.start();
	}
	public static void herirMago() {
		herirMago= Herramientas.cargarSonido("ouch.wav");
		herirMago.start();
	}
	public static void revivir() {
		revivir= Herramientas.cargarSonido("revivir.wav");
		revivir.start();
	}
	
	//Menus
	public static void gameover(){
		gameover= Herramientas.cargarSonido("gameover.wav");
		gameover.start();
	}
	public static void gameoverStop(){
		gameover.stop();
	}
	public static void menu(){
		menu.loop(1000);
	}
	public static void menuStop(){
		menu.stop();
	}
}
