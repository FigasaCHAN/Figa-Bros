package juego;


import java.util.Random;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// Escenario-Magos-Hubs-Poderes.
	private Entorno entorno;
	private Escenario escenario;
	private Mago mago1,mago2;
	private Hub hubMago1,hubMago2;
	private PoderCongelante poder,poder2;
	
	//-----Menu-PreJuego--------
	private Menu menu;
	private int seleccion;
	private boolean multiJugador;
	private boolean iniciarInterfaz;
	private boolean pantallaGameOver;
	//----Random--------
	private Random random;
	private int escenarioRandom;
	//-----Musica--------
	private boolean inicioMusica=false; //Indica si se inicio la musica. 
	private boolean inicioMusicaGO=false;
	private boolean inicioMusicaMenu=false;
	//-----------------------------
	
	public Juego(){
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "FigaBros :v", 800, 600);
		
		//Herramientas del menu y Menu
		this.menu= new Menu();
		this.seleccion=0;
		this.iniciarInterfaz=false;
		this.pantallaGameOver=false;
		this.multiJugador=false;
		
		//RANDOM
		this.random= new Random();
		
		//Objetos con interacciones.
		this.escenario= new Escenario(2);
		this.mago1= new Mago(escenario.barraInicio().getX(),0,40,15,0);
		this.mago2= new Mago(escenario.barraInicio().getX()+40,0,40,15,1);
		this.hubMago1= new Hub(1); 
		this.hubMago2= new Hub(2);
		
		// Inicia el juego
		this.entorno.iniciar();
		
	}

	
	public void tick()
	{
		//Se inicia el juego si la seleccion es 1 y se apreto Enter.
		//Si el multi jugador esta activado entonces se correra esta rama del juego.
		if (iniciarInterfaz==true && seleccion==1) {
			
			//-----SE CORRE MULTIJUGADOR----
			Sonidos.menuStop();
			inicioMusicaMenu=false; //SE REINICIA DE MUSICA DEL MENU

			if(!inicioMusica) {//Controla que entra una sola vez
				escenario.play(escenario.getNumeroEscenario());
				inicioMusica=true;//SE INICIA
			}
			
			//------DIBUJAR-----
			escenario.dibujarEscenario(entorno);
			
			if (!mago1.estaMuerto()) {
				mago1.dibujarMago0(entorno); 
			}
			if (!mago2.estaMuerto()) {
				mago2.dibujarMago1(entorno);
			}
			
			hubMago1.dibujarVidas(entorno,mago1);
			hubMago1.dibujarPuntos(entorno,mago1);
			hubMago2.dibujarVidas(entorno, mago2);

			multiJugador=true; //Esta variable se cambia aqui para indicar que se esta corriendo el multi.
			
			//MOVIMIENTOS DEL MAGO 1
			if (!mago1.chocaConBarraIzquierda(escenario) && !mago1.estaMuerto() ) {
				if (entorno.estaPresionada(entorno.TECLA_DERECHA) && !escenario.chocasteConVigas(mago1)
						&& (!escenario.estaEnVacio(mago1) || mago1.magoTienePocionVoladora())) {
					mago1.moverDerecha(mago1.getID());
				}
			}
			if (!mago1.chocaConBarraDerecha(escenario) && !mago1.estaMuerto() ) {
				if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !escenario.chocasteConVigas(mago1)
						&& (!escenario.estaEnVacio(mago1) || mago1.magoTienePocionVoladora())) {
					mago1.moverIzquierda(mago1.getID());
				}
			}
			//DETENER MOVIMIENTO DEL MAGO 1
			if (!entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				mago1.detenerseMago0(mago1.getDireccion());
			}

			//MOVIMIENTO DEL MAGO 2
			if (!mago2.chocaConBarraIzquierda(escenario) && !mago2.estaMuerto()) {
				if (entorno.estaPresionada('d') && !escenario.chocasteConVigas(mago2)
						&& (!escenario.estaEnVacio(mago2) || mago2.magoTienePocionVoladora())) {
					mago2.moverDerecha(mago2.getID());
				}
			}
			if (!mago2.chocaConBarraDerecha(escenario) && !mago2.estaMuerto()) {
				if (entorno.estaPresionada('a') && !escenario.chocasteConVigas(mago2)
						&& (!escenario.estaEnVacio(mago2) || mago2.magoTienePocionVoladora())) {
					mago2.moverIzquierda(mago2.getID());
				}
			}
			//DETENER MOVIMIENTO DEL MAGO 2
			if (!entorno.estaPresionada('a') && !entorno.estaPresionada('d')) {
				mago2.detenerseMago1(mago2.getDireccion());
			}
			
			//-------DISPARO-MAGO-1-------
			/*Si la Y del mago es igual a la Y de donde disparo
			 * significa que el mago no se movio de la viga de donde disparo con antelacion
			 * al mas minimo movimiento en "Y" del mago significa que se movio
			 * por ende ahora el disparo volvera a estar disponible de nuevo.
			 */
			if (mago1.posicionDisparo()!=mago1.getY() && !mago1.estaMuerto() && !mago1.magoTienePocionPoderControlado()) {  
				if (poder==null && !escenario.estaEnVacio(mago1)) {
					poder= new PoderCongelante(mago1,1);
					Sonidos.disparo();
					mago1.guardarPosicion(); //Guarda la posicion de las Y de donde se disparo.
				}else {
					mago1.resetPosicionDisparo();//Se resetea la posicion de disparo.
				}	
			}
			//DISPARO CON EL ITEM 
			if(mago1.magoTienePocionPoderControlado()) {
				if(poder==null && entorno.sePresiono(entorno.TECLA_ESPACIO)) {
					poder= new PoderCongelante(mago1,1);
					Sonidos.disparo();
				}
			}
			//Si el poder no es null entonces el poder avanza.
			if(poder!=null &&escenario.getImpacto()==false) {
				poder.avanzar();
				poder.dibujar(entorno);
			}
			//Si un enemigo es impactado entonces el poder se destruye.
			if (escenario.getImpacto()==true) {
				poder=null;
				escenario.setImpacto(false);
			}
			//Se detecta cuando el poder choca con las paredes.
			if (poder!=null && poder.chocasteCon(escenario)) {
				Sonidos.romperHielo();
				poder=null;
			}
			
			
			//-----DISPARO-MAGO-2-------
			if (mago2.posicionDisparo()!=mago2.getY() && !mago2.estaMuerto() && !mago2.magoTienePocionPoderControlado()) {  
				if (poder2==null && !escenario.estaEnVacio(mago2)) {
					poder2= new PoderCongelante(mago2, 2);//Se le pasa 2, porque es el jugador 2
					Sonidos.disparo();
					mago2.guardarPosicion(); //Guarda la posicion de las Y de donde se disparo.
				}else {
					mago2.resetPosicionDisparo(); //Se resetea la posicion de disparo.
				}	
			}
			//DISPARO CON EL ITEM MAGO 2
			if (mago2.magoTienePocionPoderControlado()) {
				if (poder2==null && entorno.sePresiono(entorno.TECLA_CTRL)) {
					poder2= new PoderCongelante(mago2,2);
					Sonidos.disparo();
				}
			}

			//Si el poder no es null entonces el poder avanza.
			if(poder2!=null &&escenario.getImpacto2()==false) {
				poder2.avanzar();
				poder2.dibujar(entorno);
			}
			//Si un enemigo es impactado entonces el poder se destruye.
			if (escenario.getImpacto2()==true) {
				poder2=null;
				escenario.setImpacto2(false); //Crear otro impacto para el mago 2
			}
			//Se detecta cuando el poder choca con las paredes.
			if (poder2!=null && poder2.chocasteCon(escenario)) {
				Sonidos.romperHielo();
				poder2=null;
			}
			
			//SE DETECTA SI EL MAGO 1 SE QUEDA QUIETO. Y COMIENZA A CAER.
			if (escenario.estaEnVacio(mago1)) {
				mago1.caer(); 
				mago1.detenerseMago0(mago1.getDireccion());  
			}
			//SE DETECTA SI EL MAGO 2 SE QUEDA QUIETO. Y COMIENZA A CAER.
			if (escenario.estaEnVacio(mago2)) {
				mago2.caer();
				mago2.detenerseMago1(mago2.getDireccion()); 
			}
			
			//-----------CAMBIO DE ESCENARIO------
			if (escenario.noHayMasEnemigos() && (mago1.llegasteAlFinal(escenario) || mago2.llegasteAlFinal(escenario))) {
				escenarioRandom = random.nextInt(3);
				escenario.stop(escenario.getNumeroEscenario());//Se para la musica actual
				escenario = new Escenario(escenarioRandom);//Se crea el proximo escenario
				escenario.play(escenario.getNumeroEscenario());//Se reproduce la cancion del siguien escenario.
			}
			
			//---------SI-LOS-MAGOS-CAEN-AL-VORTEX-EMPIEZAN-ARRIBA-DE-NUEVO--------------
			if (mago1.llegasteAlFinal(escenario)) {
				mago1.volverAInicio();
			}
			if (mago2.llegasteAlFinal(escenario)) {
				mago2.volverAInicio();
			}
		
			//------Congelar-Enemigos------------
			escenario.congelarEnemigos(poder,multiJugador);
			escenario.congelarEnemigos(poder2,multiJugador);
			
			
			//--------ENEMIGOS-RODANDO-QUE-CAEN-EN-EL-VORTEX-----
			
			/*Se le pasa como parametro el mago para detectar el sistemas de puntos
			 * Cuando un enemigo que esta rodando toca el agujero del centro entonces dicho enemigo morira
			 * y con esta accion se le sumaran puntos al mago.
			 */
			escenario.enemigosLlegaronAlFinalMulti(mago1, mago2); 

			//---------Enemigos-Que-Ruedan-----------
			escenario.magoChocasteConEnemigosCongelados(mago1);  //Se detecta si el mago choco con los enemigos congelados.
			escenario.magoChocasteConEnemigosCongelados(mago2);
			escenario.estadoDelEnemigoQueEstaRodando(); //Con esto se actualiza el movimiento de los enemigos que estan rodando.
			escenario.enemigosRodandoChocaronConEnemigos(); //Detecta si un enemigo rodando choca a algun enemigo normal.
			//----------------------------------

			if(escenario.noHayMasEnemigos()) {
				//----PocionVoladora--
				escenario.apareceItemPocionVoladoraMulti();//APARECE PocionVoladora
				escenario.desaparecePocionVoladoraEnMulti(1);//RECIBE 1, QUE INDICA UN TICK.. VA A DESAPARECER EN 300 TICK
				escenario.magosAgarranItemPocionVoladora(mago1,mago2);//COMPRUEBA SI EL MAGO AGARRA EL ITEM O NO
				//----PocionPoderControlado----
				escenario.apareceItemPocionPoderControladoMulti();
				escenario.desaparecePocionPoderControladoMulti(1);
				escenario.magosAgarranItemPocionPoderControlado(mago1,mago2);
			}
			
			//-----COMPRA-DE-ITEMS---
			if(entorno.estaPresionada('i') && entorno.sePresiono('o')) { //Comprar PocionVoladora.
				mago1.magoCompraPocionVoladora();
				mago2.magoCompraPocionVoladora();
			}
			if(entorno.estaPresionada('i') && entorno.sePresiono('p')) {  //Comprar PocionPoderControlado.
				mago1.magoCompraPocionPoderControlado();
				mago2.magoCompraPocionPoderControlado();
			}
			if( entorno.estaPresionada('i') && entorno.sePresiono('l')) { //Comprar vidas o "resucitar" a algun mago muerto.
				if(mago1.estaMuerto() || mago2.estaMuerto()) {
					Sonidos.revivir();
					mago1.magoRevive();
					mago2.magoRevive();
				}
			}
			
			//-------Sistema-De-Vidas-------
			if (escenario.magoChocasteConEnemigos(mago1)) {
				Sonidos.herirMago();
				mago1.volverAlInicioSinUnaVida(); //Si el enemigo no esta congelado va a quitarle vida al mago
			}
			if (escenario.magoChocasteConEnemigos(mago2)) {
				Sonidos.herirMago();
				mago2.volverAlInicioSinUnaVida();
			}
			
			if(mago1.estaMuerto() && mago2.estaMuerto()) {
				escenario.stop(escenario.getNumeroEscenario());//Se detiene la musica.
				//Si dispararon antes de morir, se elimina el disparo.
				poder=null;
				poder2=null;

				pantallaGameOver=true; //GAME OVER
				iniciarInterfaz=false; //"VUELVE AL MENU"
				
				escenarioRandom = random.nextInt(3);
				escenario= new Escenario(escenarioRandom); //CREA EL PROXIMO ESCENARIO QUE SE VA A JUGAR
				mago1= new Mago(escenario.barraInicio().getX(), escenario.barraInicio().getY()- escenario.barraInicio().getAlto()/2 -15,40,15,0); //VUELVE A CREAR A LOS MAGOS
				mago2= new Mago(escenario.barraInicio().getX()+40/2, escenario.barraInicio().getY()- escenario.barraInicio().getAlto()/2 -15,40,15,1);	 
			}
			
			//------Volver-Al-Menu---------
			if (entorno.sePresiono(entorno.TECLA_FIN)) {
				poder=null; //Si dispararon antes de morir, se elimina el disparo.
				poder2=null;
				
				escenario.stop(escenario.getNumeroEscenario());//Se detiene la musica
				//Se reinician los magos.
				mago1.reiniciar();
				mago2.reiniciar();
				iniciarInterfaz=false;
				pantallaGameOver=true;
				
				escenarioRandom = random.nextInt(3);
				escenario= new Escenario(escenarioRandom);//Se crea otro escenario
				
				mago1= new Mago(escenario.barraInicio().getX(),0,40,15,0);//Se reposiciona el mago1.
				mago2= new Mago(escenario.barraInicio().getX()+40,0,40,15,1);//Se reposiciona el mago2.
			}


		}else if (iniciarInterfaz==true && seleccion==0){
			
			//-----INICIO SINGLEPLAYER-----
			Sonidos.menuStop();//Significa que entro al single-player y ya no esta en el menu
			inicioMusicaMenu=false;
			
			
			escenario.dibujarEscenario(entorno);
			
			if(!inicioMusica) {//Controla que entra una sola vez
				escenario.play(escenario.getNumeroEscenario());//Se inicia la musica 
				inicioMusica=true;
			}
			
			
			hubMago1.dibujarVidas(entorno,mago1);
			hubMago1.dibujarPuntos(entorno,mago1); 
			mago1.dibujarMago0(entorno);

			multiJugador=false;
			
			//------MOVIMIENTOS DEL MAGO------
			if (!mago1.chocaConBarraIzquierda(escenario)) {
				if (entorno.estaPresionada(entorno.TECLA_DERECHA) && !escenario.chocasteConVigas(mago1) 
						&& (!escenario.estaEnVacio(mago1) || mago1.magoTienePocionVoladora()) ) {
					mago1.moverDerecha(mago1.getID());
				}
			}
			if (!mago1.chocaConBarraDerecha(escenario)) {
				if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !escenario.chocasteConVigas(mago1)
						&&(!escenario.estaEnVacio(mago1) || mago1.magoTienePocionVoladora())
						) {
					mago1.moverIzquierda(mago1.getID());
				}
			}
			
			//DETENER MOVIMIENTOS.
			if (!entorno.estaPresionada(entorno.TECLA_DERECHA) && !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
				mago1.detenerseMago0(mago1.getDireccion());
			}
			
			//DETECCION DE GAME OVER.
			if(mago1.estaMuerto()) {
				poder=null; //Se elimina el disparo.
				pantallaGameOver=true;
				iniciarInterfaz=false;
				escenario.stop(escenario.getNumeroEscenario());//Si muere, se quita la musica
				
				escenarioRandom = random.nextInt(3);
				escenario= new Escenario(escenarioRandom);
				
				mago1= new Mago(escenario.barraInicio().getX(), 0 ,40,15,0); //Se reposiciona el mago1.
			}

			//"GRAVEDAD" DEL MAGO
			if (escenario.estaEnVacio(mago1)) {
				mago1.caer(); 
				mago1.detenerseMago0(mago1.getDireccion()); //El mago deja de moverse mientras cae.
			}

			//CAMBIO DE ESCENARIO
			if (escenario.noHayMasEnemigos() && mago1.llegasteAlFinal(escenario)) {
				escenarioRandom = random.nextInt(3);
				escenario.stop(escenario.getNumeroEscenario());//Se para la musica actual
				escenario = new Escenario(escenarioRandom);//Se crea el proximo escenario
				
				escenario.play(escenario.getNumeroEscenario());//Se reproduce la siguiente cancion.
			}
			
			//VOLVER AL INICIO CUANDO EL MAGO TERMINE EL "RECORRIDO"
			if (mago1.llegasteAlFinal(escenario)) {
				mago1.volverAInicio();
			}

			//-------Sistema-De-Vidas-------
			if (escenario.magoChocasteConEnemigos(mago1)) {
				Sonidos.herirMago();
				mago1.volverAlInicioSinUnaVida(); //Si el enemigo no esta congelado le va a quitar vida al mago
			}
			
			//------Volver-Al-Menu---------
			if (entorno.sePresiono(entorno.TECLA_FIN)) {
				mago1.reiniciar(); //Se resetea el mago1.
			}
			
			//-------Disparo-------
			/*Si la Y del mago es igual a la Y de donde disparo
			 * significa que el mago no se movio de la viga de donde disparo con antelacion
			 * al mas minimo movimiento en "Y" del mago significa que se movio
			 * por ende ahora el disparo volvera a estar disponible de nuevo.
			 */
			if (mago1.posicionDisparo()!=mago1.getY()  && !mago1.magoTienePocionPoderControlado()) {  
				if (poder==null && !escenario.estaEnVacio(mago1)) {
					poder= new PoderCongelante(mago1,1);
					Sonidos.disparo();
					mago1.guardarPosicion(); //Guarda la posicion en la cual se disparo el poder.
				}else {
					mago1.resetPosicionDisparo(); //Se resetea la posicion de disparo.
				}	
			}
			//DISPARO CON EL ITEM 
			if(mago1.magoTienePocionPoderControlado()) {
				if(poder==null && entorno.sePresiono(entorno.TECLA_ESPACIO)) {
					poder= new PoderCongelante(mago1,1);
					Sonidos.disparo();
				}
			}

			//Si el poder no es null entonces el poder avanza.
			if(poder!=null &&escenario.getImpacto()==false) {
				poder.avanzar();
				poder.dibujar(entorno);
			}
			//Si un enemigo es impactado entonces el poder se destruye.
			if (escenario.getImpacto()==true) {
				poder=null;
				escenario.setImpacto(false);
			}
			//Se detecta cuando el poder choca con las paredes.
			if (poder!=null && poder.chocasteCon(escenario)) {
				Sonidos.romperHielo();
				poder=null;
			}


			//------Congelar-Enemigos------------
			escenario.congelarEnemigos(poder,multiJugador);
			
			
			//SE DETECTA SI LOS ENEMIGOS QUE ESTAN RODANDO LLEGARON AL FINAL

			/*Se le pasa como parametro el mago para detectar el sistemas de puntos
			 * Cuando un enemigo que esta rodando toca el agujero del centro entonces dicho enemigo morira
			 * y con esta accion se le sumaran puntos al mago.
			 */
			escenario.enemigosLlegaronAlFinal(mago1);  
		
			//---------Enemigos-Que-Ruedan-----------
			escenario.magoChocasteConEnemigosCongelados(mago1);  //Se detecta si el mago choco con los enemigos congelados.
			escenario.estadoDelEnemigoQueEstaRodando(); //Con esto se actualiza el movimiento de los enemigos que estan rodando.
			escenario.enemigosRodandoChocaronConEnemigos(); //Detecta si un enemigo rodando choca a algun enemigo normal.
			
			//--------ITEMS----
			if(escenario.noHayMasEnemigos()) {
				//----POCION VOLADORA--
				escenario.apareceItemPocionVoladora();//APARECE PocionVoladora
				escenario.desaparecePocionVoladoraEn(1);//RECIBE 1, QUE INDICA UN TICK.. VA A DESAPARECER EN 300 TICK
				escenario.magoAgarraItemPocionVoladora(mago1);//COMPRUEBA SI EL MAGO AGARRA EL ITEM O NO
				//----POCION PODER CONTROLADO----
				escenario.apareceItemPocionPoderControlado();
				escenario.desaparecePocionPoderControlado(1);
				escenario.magoAgarraItemPocionPoderControlado(mago1);
			}
			
			//-----COMPRA DE ITEMS---
			if(entorno.estaPresionada('i') && entorno.sePresiono('o')) {
				mago1.magoCompraPocionVoladora();
			}
			if(entorno.estaPresionada('i') && entorno.sePresiono('p')) {
				mago1.magoCompraPocionPoderControlado();
			}
			
		}else if (iniciarInterfaz==true && seleccion==2) {
			// SALIR DEL PROGRAMA
			System.exit(0);  
		}else if (pantallaGameOver==true) {  
			
			//-------MENU GAME OVER------
			if(!inicioMusicaGO) { //Se reproduce una sola vez
				Sonidos.gameover();
				inicioMusicaGO=true; 
			}
			menu.dibujarGameOver(entorno);

			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				//VUELVE AL MENU PRINCIPAL
				Sonidos.gameoverStop(); //Se detiene la musica del game over
				pantallaGameOver=false;
				iniciarInterfaz=false;
				inicioMusica=false;//Se reinicia toda la musica
				inicioMusicaGO=false;
			}
			
		}else {  
			//MENU DE INICIO
			
			if(!inicioMusicaMenu) {
				Sonidos.menu();
				inicioMusicaMenu=true;
			}
			
			menu.dibujarMenuDeInicio(entorno);
			menu.mostrarOpciones(entorno);
			
			//Se detecta si el usuario desea moverse para arriba en las opciones del menu y si tambien puede desplazar hacia arriba
			//"seleccion>0" estaria "bloqueando" el movimiento de la seleccion para que el usuario no vaya 20 lugares arriba por ejemplo.
			if (entorno.sePresiono(entorno.TECLA_ARRIBA) && seleccion>0) {
				seleccion--;
				menu.alternarOpcionesHaciaArriba(seleccion);
			}
			
			if (entorno.sePresiono(entorno.TECLA_ABAJO) && seleccion<2) {
				seleccion++;
				menu.alternarOpcionesHaciaAbajo(seleccion);
			}
			//Si el usuario decidio que opcion va a ejecutar entonces el "iniciarInterfaz" pasa a ser true y se lo redireccion a la opcion elegida.
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				iniciarInterfaz=true;
			}
		}
	}
	

	@SuppressWarnings("unused")
	public static void main(String[] args){
		Juego juego = new Juego();
	}
}
