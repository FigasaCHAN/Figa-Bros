package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;

public class Escenario {
	private Viga sueloizquierdo; //suelo izquierdo
	private Viga sueloderecho;	//suelo derecho
	private Viga barraIzquierda; //Borde izquierdo.
	private Viga barraDerecha;	//Borde derecho.
	private Viga barra1,barra2,barra3,barra4,barra5,barra6,barra7,barra8,barra9,barra10,barra11;
	public Viga agujero;
	private Viga [] todas; //voy a poner 12, 10 barras y los suelos
	private EnemigosBase enemigob1,enemigob2,enemigob3,enemigob4,enemigob5,enemigob6,enemigob7,enemigob8,enemigob9,enemigob10; 
	private EnemigosBase[] todosLosEnemigosBase;
	private Image fondo;
	private boolean impacto=false, impacto2=false;
	private int cantidadDeEnemigos;
	private PocionVoladora pocionVoladoraEnEscenario, pocion2VoladoraEnEscenario; 
	private int pocionVoladoraAparecio, pocion2VoladoraAparecio; //variable que me va a indicar cuantas veces va a aparecer la pocionVoladora.
	private PocionPoderControlado pocionPoderControladoEnEscenario, pocion2PoderControladoEnEscenario; //poderRafaga2EnEscenario;
	private int pocionPoderControladoAparecio, pocion2PoderControladoAparecio;
	private static Clip musica0= Herramientas.cargarSonido("tokyoGhoul.wav"); 
	private static Clip musica1=Herramientas.cargarSonido("heManMusic.wav");
	private static Clip musica2= Herramientas.cargarSonido("evangelion.wav");
	private int numeroEscenario;
	
	private static Image fondo0= Herramientas.cargarImagen("fondoTorre.gif");
	private static Image fondo1= Herramientas.cargarImagen("fondoCielo.gif");
	private static Image fondo2= Herramientas.cargarImagen("fondoCielo2.gif");
	
	
	private static Image vigaPiedraGrande= Herramientas.cargarImagen("vigaPiedraGrande.png");
	private static Image vigaPiedraChica= Herramientas.cargarImagen("vigaPiedraChica.png");
	private static Image vigaParedes= Herramientas.cargarImagen("vigaParedes.png");
	private static Image vortex= Herramientas.cargarImagen("vortex.gif");

	
	
	//Se crea el constructor.
	public Escenario(int x) {
		if (x==0) {
			
			
			//Barras que sostienen al jugador.
			this.sueloizquierdo= new Viga(150, 590, 30, 300, vigaPiedraGrande); //Coordenadas adaptadas a 800x600
			this.sueloderecho = new Viga(650, 590, 30, 300, vigaPiedraGrande);
			
			this.fondo= fondo0;
			
			
			this.barra1 = new Viga(400, 165, 15, 220,vigaPiedraGrande); //barra comienzo
			this.barra2 = new Viga(250, 250, 15, 145, vigaPiedraChica); //barra izquierda, 3ra fila
			this.barra3 = new Viga(550, 250, 15, 145, vigaPiedraChica); //barra derecha, 3ra fila
			this.barra4 = new Viga(100, 330, 15, 220, vigaPiedraGrande);//barra centro 3ra fila
			this.barra5 = new Viga(700, 330, 15, 220,vigaPiedraGrande); //barra izquierda 2da fila
			this.barra6 = new Viga(400, 330, 15, 220,vigaPiedraGrande); //barra derecha 2da fila
			this.barra7 = new Viga(250, 420, 15, 220,vigaPiedraGrande);//barra centro 2da fila
			this.barra8 = new Viga(550, 420, 15, 220,vigaPiedraGrande); //barra centro, 4ta fila
			this.barra9 = new Viga(100, 500, 15, 220, vigaPiedraGrande); //barra izquierda, 4ta fila
			this.barra10 = new Viga(700, 500, 15, 220, vigaPiedraGrande); //barra derecha, 4ta fila
			
			this.agujero= new Viga(400, 650, 60, 200, vortex);//Vortex o final del escenario.
			
			//Bordes de la pantalla
			this.barraIzquierda= new Viga(35, 400, 800, 10, vigaParedes);
			this.barraDerecha= new Viga(775, 400, 800, 10,  vigaParedes);
		
			this.todas= new Viga[12];
		
			//Enemigos base
			this.todosLosEnemigosBase= new EnemigosBase[6];
			
			this.enemigob1= new EnemigosBase(barra7.getX(),barra7.getY()-barra7.getAlto()/2-15,30,20);
			this.enemigob2= new EnemigosBase(barra2.getX(),barra2.getY()-barra2.getAlto()/2-15,30,20);
			this.enemigob3= new EnemigosBase(barra3.getX(),barra3.getY()-barra3.getAlto()/2-15,30,20);
			this.enemigob4= new EnemigosBase(barra4.getX(),barra4.getY()-barra4.getAlto()/2-15,30,20);
			this.enemigob5= new EnemigosBase(barra5.getX(),barra5.getY()-barra5.getAlto()/2-15,30,20);
			this.enemigob6= new EnemigosBase(barra6.getX(),barra6.getY()-barra6.getAlto()/2-15,30,20);
			
			//Lista
			todas[0]= barra1;
			todas[1]= barra2;
			todas[2]= barra3;
			todas[3]= barra4;
			todas[4]= barra5;
			todas[5]= barra6;
			todas[6]= barra7;
			todas[7]= barra8;
			todas[8]= barra9;
			todas[9]= barra10;
			todas[10]= sueloizquierdo;
			todas[11]= sueloderecho;
			
			
			//Lista de los EnemigosBase
			todosLosEnemigosBase[0]= enemigob1;
			todosLosEnemigosBase[1]= enemigob2;
			todosLosEnemigosBase[2]= enemigob3;
			todosLosEnemigosBase[3]= enemigob4;
			todosLosEnemigosBase[4]= enemigob5;
			todosLosEnemigosBase[5]= enemigob6;
			cantidadDeEnemigos= todosLosEnemigosBase.length;
			
			this.numeroEscenario=x; //NUMERO QUE ME INDICA QUE ESCENARIO SE ESTA JUGANDO
			
		}else if(x==1) {  
			
			//Barras que sostienen al jugador.
			this.sueloizquierdo= new Viga(150, 605, 60, 300, vigaPiedraGrande); //coordenadas adaptadas a 800x600
			this.sueloderecho = new Viga(650, 605, 60, 300, vigaPiedraGrande);
			
			this.fondo= fondo1;
			
			this.barra1 = new Viga(400, 160, 15, 220, vigaPiedraGrande); //barra comienzo
			this.barra2 = new Viga(230, 245, 15, 220, vigaPiedraGrande); //barra izquierda, 3ra fila
			this.barra3 = new Viga(570, 245, 15, 220, vigaPiedraGrande); //barra derecha, 3ra fila
			this.barra4 = new Viga(50, 330, 15, 220, vigaPiedraGrande);//barra centro 3ra fila
			this.barra5 = new Viga(400, 330, 15, 220,vigaPiedraGrande); //barra izquierda 2da fila
			this.barra6 = new Viga(720, 330, 15, 220,vigaPiedraGrande); //barra derecha 2da fila
			this.barra7 = new Viga(230, 415, 15, 220, vigaPiedraGrande);//barra centro 2da fila
			this.barra8 = new Viga(560, 415, 15, 220,vigaPiedraGrande); //barra centro, 4ta fila
			this.barra9 = new Viga(50, 500, 15, 220, vigaPiedraGrande); //barra izquierda, 4ta fila
			this.barra10 = new Viga(750, 500, 15, 220, vigaPiedraGrande); //barra derecha, 4ta fila
			this.barra11 = new Viga(400,500,15,220,vigaPiedraGrande);
			
			this.agujero= new Viga(400, 650, 60, 200, vortex);//Vortex o final del escenario  
			//Bordes de la pantalla
			this.barraIzquierda= new Viga(35, 400, 800, 10, vigaPiedraGrande);
			this.barraDerecha= new Viga(775, 400, 800, 10,vigaPiedraGrande);
			
			this.todas= new Viga[13];
		
			//Enemigos base
			this.todosLosEnemigosBase= new EnemigosBase[6];
			
			this.enemigob1= new EnemigosBase(barra2.getX()+50,barra2.getY()-barra1.getAlto()/2-30,45,30);
			this.enemigob2= new EnemigosBase(barra2.getX(),barra2.getY()-barra2.getAlto()/2-30,45,30);
			this.enemigob3= new EnemigosBase(barra3.getX(),barra3.getY()-barra3.getAlto()/2-30,45,30);
			this.enemigob4= new EnemigosBase(barra4.getX(),barra4.getY()-barra4.getAlto()/2-30,45,30);
			this.enemigob5= new EnemigosBase(barra5.getX(),barra5.getY()-barra5.getAlto()/2-30,45,30);
			this.enemigob6= new EnemigosBase(barra6.getX(),barra6.getY()-barra6.getAlto()/2-30,45,30);
			//Lista
			todas[0]= barra1;
			todas[1]= barra2;
			todas[2]= barra3;
			todas[3]= barra4;
			todas[4]= barra5;
			todas[5]= barra6;
			todas[6]= barra7;
			todas[7]= barra8;
			todas[8]= barra9;
			todas[9]= barra10;
			todas[10]= barra11;
			todas[11]= sueloizquierdo;
			todas[12]= sueloderecho;
			
			//Lista de los EnemigosBase
			todosLosEnemigosBase[0]= enemigob1;
			todosLosEnemigosBase[1]= enemigob2;
			todosLosEnemigosBase[2]= enemigob3;
			todosLosEnemigosBase[3]= enemigob4;
			todosLosEnemigosBase[4]= enemigob5;
			todosLosEnemigosBase[5]= enemigob6;
			cantidadDeEnemigos= todosLosEnemigosBase.length;
			
			this.numeroEscenario=x;
			
		}else if(x==2) {  
			
			
			//Barras que sostienen al jugador.
			this.sueloizquierdo= new Viga(150, 610, 60, 300, vigaPiedraGrande); //coordenadas adaptadas a 800x600
			this.sueloderecho = new Viga(650, 610, 60, 300, vigaPiedraGrande);
			
			this.fondo= fondo2;
			
		
			this.barra1 = new Viga(400, 160, 15, 220, vigaPiedraGrande); //Barra comienzo
			this.barra2 = new Viga(200, 245, 15, 220, vigaPiedraGrande); 
			this.barra3 = new Viga(600, 245, 15, 220,vigaPiedraGrande); 
			this.barra4 = new Viga(50, 330, 15, 220, vigaPiedraGrande);
			this.barra5 = new Viga(400, 330, 15, 220, vigaPiedraGrande);
			this.barra6 = new Viga(720, 330, 15, 220,vigaPiedraGrande); 
			this.barra7 = new Viga(100, 415, 15, 220,vigaPiedraGrande);
			this.barra8 = new Viga(560, 415, 15, 220,vigaPiedraGrande);
			this.barra9 = new Viga(50, 500, 15, 220, vigaPiedraGrande); 
			this.barra10 = new Viga(750, 500, 15, 220, vigaPiedraGrande); 
			this.barra11 = new Viga(400,500,15,220,vigaPiedraGrande);
			
			this.agujero= new Viga(400, 650, 60, 220, vortex);//Vortex o final del escenario
			
			//Bordes de la pantalla
			this.barraIzquierda= new Viga(35, 400, 800, 10, vigaPiedraGrande);
			this.barraDerecha= new Viga(775, 400, 800, 10,vigaPiedraGrande);
			
			this.todas= new Viga[13];
		
			//Enemigos base
			this.todosLosEnemigosBase= new EnemigosBase[10];
			
			this.enemigob1= new EnemigosBase(barra2.getX()+50,barra2.getY()-barra1.getAlto()/2-30,45,30);
			this.enemigob2= new EnemigosBase(barra2.getX(),barra2.getY()-barra2.getAlto()/2-30,45,30);
			this.enemigob3= new EnemigosBase(barra3.getX(),barra3.getY()-barra3.getAlto()/2-30,45,30);
			this.enemigob4= new EnemigosBase(barra4.getX(),barra4.getY()-barra4.getAlto()/2-30,45,30);
			this.enemigob5= new EnemigosBase(barra5.getX(),barra5.getY()-barra5.getAlto()/2-30,45,30);
			this.enemigob6= new EnemigosBase(barra6.getX(),barra6.getY()-barra6.getAlto()/2-30,45,30);
			this.enemigob7= new EnemigosBase(barra10.getX(),barra10.getY()-barra10.getAlto()/2-30,45,30);
			this.enemigob8= new EnemigosBase(barra8.getX(),barra8.getY()-barra8.getAlto()/2-30,45,30);
			this.enemigob9= new EnemigosBase(barra7.getX(),barra7.getY()-barra7.getAlto()/2-30,45,30);
			this.enemigob10= new EnemigosBase(barra11.getX(),barra11.getY()-barra11.getAlto()/2-30,45,30);
			
			
			//Lista
			todas[0]= barra1;
			todas[1]= barra2;
			todas[2]= barra3;
			todas[3]= barra4;
			todas[4]= barra5;
			todas[5]= barra6;
			todas[6]= barra7;
			todas[7]= barra8;
			todas[8]= barra9;
			todas[9]= barra10;
			todas[10]= barra11;
			//todas[11]= barraPreso;
			todas[11]= sueloizquierdo;
			todas[12]= sueloderecho;
			
			//Lista de los EnemigosBase
			todosLosEnemigosBase[0]= enemigob1;
			todosLosEnemigosBase[1]= enemigob2;
			todosLosEnemigosBase[2]= enemigob3;
			todosLosEnemigosBase[3]= enemigob4;
			todosLosEnemigosBase[4]= enemigob5;
			todosLosEnemigosBase[5]= enemigob6;
			todosLosEnemigosBase[6]= enemigob7;
			todosLosEnemigosBase[7]= enemigob8;
			todosLosEnemigosBase[8]= enemigob9;
			todosLosEnemigosBase[9]= enemigob10;
			
			cantidadDeEnemigos= todosLosEnemigosBase.length;
			
			numeroEscenario=x;
		}
	}
	public void dibujarEscenario(Entorno entorno) { //Se dibuja el escenario

		//Fondo
		entorno.dibujarImagen(fondo, 405, 310, 0);
		
		//Vortex
		agujero.dibujarVigaConImagen(entorno);

		//Barras
		for (int i=0;i<todas.length-2;i++) {
			todas[i].dibujarVigaConImagen(entorno);
		}
		for (int i=0;i<cantidadDeEnemigos; i++) {
			if (todosLosEnemigosBase[i]!=null) {
				todosLosEnemigosBase[i].dibujarEnemigo(entorno);
			}
		}
		
		//----GRAVEDAD---
		
		gravedad();
		
		//----ITEMS---
		if(pocionVoladoraEnEscenario!=null) { //SI EXISTE ALGUNA POCION, SE DIBUJA
			pocionVoladoraEnEscenario.dibujarPocionVoladora(entorno);
		}
		if(pocion2VoladoraEnEscenario!=null) {
			pocion2VoladoraEnEscenario.dibujarPocionVoladora(entorno);
		}
		if(pocionPoderControladoEnEscenario!=null) {
			pocionPoderControladoEnEscenario.dibujarPocionPoderControlado(entorno);
		}
		if(pocion2PoderControladoEnEscenario!=null) {
			pocion2PoderControladoEnEscenario.dibujarPocionPoderControlado(entorno);
		}
	}
	
	//-----FUNCIONES BASICAS DE ESCENARIO
	public boolean estaEnVacio(Mago mago) {//SI ES TRUE, EL MAGO ESTA EN EL VACIO
		for (int i=0; i<todas.length; i++) {
			if(mago.agujero( todas[i]) == false) {
				return false;
			}
		}
		return true;
	}
	
	public boolean estaEnVacioEnemigoB(EnemigosBase enemigo) {//SI ES TRUE, EL ENEMIGO ESTA EN EL VACIO
		for (int i=0; i<todas.length; i++) {
			if(enemigo.agujero(todas[i]) == false) {
				return false;
			}
		}
		return true;
	}
	
	public void gravedad() {//GRAVEDAD DE LOS ENEMIGOS
		for (int i=0; i<cantidadDeEnemigos; i++) {
			if(estaEnVacioEnemigoB(todosLosEnemigosBase[i])) {
				todosLosEnemigosBase[i].caer();
			}
		}
	}
	public void OrdenarArrayDeEnemigos() {//Ordena los nulls del arreglo de enemigos
		for(int i=0; i<todosLosEnemigosBase.length-1; i++) {
			if(todosLosEnemigosBase[i]==null && todosLosEnemigosBase[i+1]!=null ) {
				EnemigosBase aux= todosLosEnemigosBase[i+1];
				todosLosEnemigosBase[i]=aux;
				todosLosEnemigosBase[i+1]=null;
			}	
		}
	}
	
	
	//GET Y SET
	public Viga getbarraIzquierda() {
		return barraIzquierda;
	}
	public Viga getbarraDerecha() {
		return barraDerecha;
	}
	public boolean getImpacto() {
		return impacto;
	}
	public void setImpacto(boolean x) {
		impacto=x;
	}
	public boolean getImpacto2() {
		return impacto2;
	}
	public void setImpacto2(boolean x) {
		impacto2=x;
	}
	
	//------MAGO-----
	public boolean magoChocasteConEnemigos(Mago mago) { //Detecta si el mago choco con un enemigo no congelado.
		for (int i=0;i<cantidadDeEnemigos;i++) {
			if (todosLosEnemigosBase[i].chocasteCon(mago) && todosLosEnemigosBase[i].getCongelado()==0 && todosLosEnemigosBase[i].getRodar()==0) {
				return true;
			}
		}
		return false;
	}
	
	public void magoChocasteConEnemigosCongelados(Mago mago) { //Detecta si el mago choco con un enemigo que si esta congelado.
		for (int i=0;i<cantidadDeEnemigos;i++) {
			if (todosLosEnemigosBase[i].chocasteCon(mago) && todosLosEnemigosBase[i].getCongelado()>0 &&todosLosEnemigosBase[i].getRodar()==0) {
				todosLosEnemigosBase[i].setDireccionRodar(mago.getDireccion()); //Se le da la direccion a la que debe dirigirse el enemigo.
				todosLosEnemigosBase[i].rodar();
			}
		}
	}
	//--------------------------------------------
	public void magoAgarraItemPocionVoladora(Mago mago) {
		if(pocionVoladoraEnEscenario!=null) {
			if(mago.magoChocaConPocionVoladora(pocionVoladoraEnEscenario)) {//EL MAGO CHOCA CON LA PocionVoladora
				mago.activarPocionVoladora();//MAGO RECIBE PocionVoladora
				System.out.println("nigga");
				pocionVoladoraEnEscenario=null;//ELIMINO LA PocionVoladora DEL ESCENARIO
				return;
			}
		}
	}
	public void magosAgarranItemPocionVoladora(Mago mago1, Mago mago2) {
		//SI CUALQUIERA DE LOS 2 MAGOS CHOCA CON LA POCION 1
		if(pocionVoladoraEnEscenario!=null) {
			if(mago1.magoChocaConPocionVoladora(pocionVoladoraEnEscenario)) {
				mago1.activarPocionVoladora();
				pocionVoladoraEnEscenario=null;
				return;
			}
			if (mago2.magoChocaConPocionVoladora(pocionVoladoraEnEscenario)) {
				mago2.activarPocionVoladora();
				pocionVoladoraEnEscenario=null;
				return;
			}
		}
		//SI CUALQUIERA DE LOS 2 MAGOS CHOCA CON LA POCION 2
		if (pocion2VoladoraEnEscenario!=null) {
			if(mago1.magoChocaConPocionVoladora(pocion2VoladoraEnEscenario)) {
				mago1.activarPocionVoladora();
				pocion2VoladoraEnEscenario=null;
				return;
			}
			if (mago2.magoChocaConPocionVoladora(pocion2VoladoraEnEscenario)) {
				mago2.activarPocionVoladora();
				pocion2VoladoraEnEscenario=null;
				return;
			}
		}
	}
	
	public void magoAgarraItemPocionPoderControlado(Mago mago) {
		if(pocionPoderControladoEnEscenario!=null) {
			if(mago.magoChocaConPocionPoderControlado(pocionPoderControladoEnEscenario)) {//EL MAGO CHOCA CON LA PocionVoladora
				mago.activarPocionPoderControlado();//MAGO RECIBE PocionVoladora
				pocionPoderControladoEnEscenario=null;//ELIMINO LA PocionVoladora EL ESCENARIO
				return;
			}
		}
	}
	public void magosAgarranItemPocionPoderControlado(Mago mago1, Mago mago2) {
		if(pocionPoderControladoEnEscenario!=null) {
			if(mago1.magoChocaConPocionPoderControlado(pocionPoderControladoEnEscenario)) {//EL MAGO CHOCA CON LA PocionVoladora
				mago1.activarPocionPoderControlado();//MAGO RECIBE PocionVoladora
				pocionPoderControladoEnEscenario=null;//ELIMINO LA PocionVoladora EL ESCENARIO
				return;
			}
			if (mago2.magoChocaConPocionPoderControlado(pocionPoderControladoEnEscenario)) {
				mago2.activarPocionPoderControlado();
				pocionPoderControladoEnEscenario=null;
				return;
			}
		}
		if (pocion2PoderControladoEnEscenario!=null) {
			if (mago1.magoChocaConPocionPoderControlado(pocion2PoderControladoEnEscenario)) {
				mago1.activarPocionPoderControlado();
				pocion2PoderControladoEnEscenario=null;
				return;
			}
			if (mago2.magoChocaConPocionPoderControlado(pocion2PoderControladoEnEscenario)) {
				mago2.activarPocionPoderControlado();
				pocion2PoderControladoEnEscenario=null;
				return;
			}
		}
	}
	public boolean chocasteConVigas(Mago mago) {
		for (int i=0; i<todas.length; i++) {
			if(mago.chocasteConViga(todas[i]) == true) {
				return true;
			}
		}
		return false;
	}

	//---------ENEMIGOS-----
	public Viga vigaparado(EnemigosBase enemigo) {
		for(int i=0; i<todas.length-2; i++) {//menos dos porque no quiero contar el suelo
			if (enemigo.agujero(todas[i])==false && !elEnemigoEstaRodando(enemigo)) {//si esto da false, significa que esta sobre esa viga.
				return todas[i];//Devuelve la viga en la que esta parado
			}
		}
		return null;	//Devuelve null cuando esta sobre las vigas(suelo) o si el enemigo esta rodando
	}

	//-------FUNCION FUNDAMENTAL----
	public void congelarEnemigos(PoderCongelante poder, boolean multijugador) {
		if(multijugador==false) {
			for (int i=0; i< cantidadDeEnemigos;i++) {
				//DETECTA SI EL ENEMIGO FUE HERIDO
				if (poder!=null && todosLosEnemigosBase[i].fuisteHeridoCon(poder)) {//Se detecta si el poder choco con el enemigo.
					impacto=true;
					todosLosEnemigosBase[i].estasCongelado();  //Se cambia el estado del enemigo y pasa a estar congelado.
				}
				//PATRON DE MOVIMIENTO
				if (todosLosEnemigosBase[i].getCongelado()==0  && !estaEnVacioEnemigoB(todosLosEnemigosBase[i])) {
					if (todosLosEnemigosBase[i].getRodar()==0) { //Solo los enemigos que no estan rodando pueden hacer el patron.
						todosLosEnemigosBase[i].moverse(barraDerecha, barraIzquierda,vigaparado(todosLosEnemigosBase[i]));
					}
				}
				//RESETEO DEL PATRON DE MOVIMIENTO
				if(estaEnVacioEnemigoB(todosLosEnemigosBase[i])){
					//Si esta en el vacio, significa que ya se cumplio el patron...
					//Asi que lo reinicio para que cuando caia a otra viga distinta de la que estaba parado, y asi haga nuevamente el patron.
					todosLosEnemigosBase[i].setPatron(0);; 
				}
				//CONGELADO Y DESCONGELADO
				if(todosLosEnemigosBase[i].getCongelado()>0 && todosLosEnemigosBase[i].getCongelado()<=200) {//Se crea un timer para medir el tiempo en el cual el enemigo se quedara congelado.
					todosLosEnemigosBase[i].estasCongelado();
				}
				if (todosLosEnemigosBase[i].getCongelado()==200) {//Cuando pasen 200 centesimas de segundos el enemigo se descongela. 
					todosLosEnemigosBase[i].descongelate();
				}
			}
		}else {
			for (int i=0; i< cantidadDeEnemigos;i++) {
				//DETECTA SI ALGUNO DE LOS PODERES CHOCA CON EL ENEMIGO
				if (poder!=null && todosLosEnemigosBase[i].fuisteHeridoCon(poder)) {//Si el poder no es null y el enemigo fue alcanzado por el poder
					if(poder.getJugador()==1) {//Poder del jugador 1
						impacto=true;
					}
					if(poder.getJugador()==2) {//Poder del jugador 2
						impacto2=true;
					}
					todosLosEnemigosBase[i].estasCongelado();  //Se cambia el estado del enemigo y pasa a estar congelado
				}
				//PATRON DE MOVIMIENTO
				if (todosLosEnemigosBase[i].getCongelado()==0  && !estaEnVacioEnemigoB(todosLosEnemigosBase[i])) {
					//Solos los enemigos que no esten congelados y no esten rodando se pueden mover.
					if (todosLosEnemigosBase[i].getRodar()==0) {
						todosLosEnemigosBase[i].moverseMulti(barraDerecha, barraIzquierda,vigaparado(todosLosEnemigosBase[i]));
					}
				}
				//RESETEO DEL PATRON DE MOVIMIENTO
				if(estaEnVacioEnemigoB(todosLosEnemigosBase[i])){
					//si esta en el vacio, significa que ya se cumplio el patron
					//asi que lo reinicio para que cuando caia a otra viga distinta del suelo, haga nuevamente el patron
					todosLosEnemigosBase[i].setPatron(0);
				}
				//CONGELADO Y DESCONGELADO
				if(todosLosEnemigosBase[i].getCongelado()>0 && todosLosEnemigosBase[i].getCongelado()<=200) {//Se crea un timer para medir el tiempo en el cual el enemigo se quedara congelado.
					todosLosEnemigosBase[i].estasCongelado();
				}
				if (todosLosEnemigosBase[i].getCongelado()==200) {//Cuando pasen 200 centesimas de segundos el enemigo se descongela. 
					todosLosEnemigosBase[i].descongelate();
				}
			}
		}
	}

	//-------RODANDO----
	public boolean elEnemigoEstaRodando(EnemigosBase enemigo) { //Indica si el enemigo esta rodando
		return enemigo.getRodar()==1? true:false;
	}
	public void estadoDelEnemigoQueEstaRodando() {
		for(int i=0; i<cantidadDeEnemigos; i++) {
			if (elEnemigoEstaRodando(todosLosEnemigosBase[i])) {  //Uso la funcion que creaste.
				todosLosEnemigosBase[i].empujados(barraDerecha,barraIzquierda); //Se le pasan las 2 vigas de los costados para redirigir su trayectoria
			}
		}
	}
	public EnemigosBase enemigoQueEstaRodando() { //Busca algun enemigo que este rodando en la lista de enemigos
		for (int i=0;i<cantidadDeEnemigos;i++) {
			if (elEnemigoEstaRodando(todosLosEnemigosBase[i])){
				return todosLosEnemigosBase[i];
			}
		}
		return null;
	}
	
	public boolean enemigosRodandoChocaronConEnemigos() { 
		EnemigosBase enemigoRodando= enemigoQueEstaRodando(); //Asigno un enemigo que esta rodando a una variable
		if (enemigoRodando!=null) {  //Si el enemigo no es null pasa.
			for (int i=0;i<cantidadDeEnemigos;i++) {
				if (enemigoRodando.chocasteCon(todosLosEnemigosBase[i])) {  //Detecto si el enemigo rodando choco con un enemigo normal
					todosLosEnemigosBase[i].setDireccionRodar(enemigoRodando.getDireccionRodar());  //Le paso la direccion a la cual el nuevo enemigo rodando debe ir
					todosLosEnemigosBase[i].rodar();  //Convierto al enemigo que fue chocado en un enemigo que estara rodando
				}
			}
		}
		return false;

	}
	//-----ENEMIGOS CAEN AL PORTAL---
	public void enemigosLlegaronAlFinal(Mago mago) {
		for (int i=0;i<cantidadDeEnemigos;i++) {
			if (todosLosEnemigosBase[i].llegasteAlFinal(agujero) && !elEnemigoEstaRodando(todosLosEnemigosBase[i])) {
				todosLosEnemigosBase[i].volverAlInicio();
			}
			if(todosLosEnemigosBase[i].llegasteAlFinal(agujero) && elEnemigoEstaRodando(todosLosEnemigosBase[i])) {
				todosLosEnemigosBase[i]=null;
				OrdenarArrayDeEnemigos();
				cantidadDeEnemigos--;
				mago.sumarPuntos();
			}
		}
	}
	
	public void enemigosLlegaronAlFinalMulti(Mago mago, Mago mago2) {
		for (int i=0;i<cantidadDeEnemigos;i++) {
			if (todosLosEnemigosBase[i].llegasteAlFinal(agujero) && !elEnemigoEstaRodando(todosLosEnemigosBase[i])) {
				todosLosEnemigosBase[i].volverAlInicio();
			}
			if(todosLosEnemigosBase[i].llegasteAlFinal(agujero) && elEnemigoEstaRodando(todosLosEnemigosBase[i])) {
				todosLosEnemigosBase[i]=null;
				OrdenarArrayDeEnemigos();
				cantidadDeEnemigos--;
				mago.sumarPuntos();
				mago2.sumarPuntos();
			}
		}
	}
	//------FUNCIONES ITEMS----
	public boolean noHayMasEnemigos() {
		return cantidadDeEnemigos==0? true:false;
	}
	//------PocionVoladora-----
	
	public void apareceItemPocionVoladora() {
		if(pocionVoladoraAparecio==0) {// Si la pocion no se encuentra creada, la crea.
			pocionVoladoraEnEscenario= new PocionVoladora(150, 550, 10, 10);
			pocionVoladoraAparecio=1;
		}
		
	}
	public void apareceItemPocionVoladoraMulti() {
		if(pocionVoladoraAparecio==0 && pocion2VoladoraAparecio==0) {// Si la pocion no se encuentra creada, la crea.
			//POCION 1
			pocionVoladoraEnEscenario= new PocionVoladora(150, 550, 10, 10);
			pocionVoladoraAparecio=1;
			//POCION 2
			pocion2VoladoraEnEscenario= new PocionVoladora(550,550,10,10);
			pocion2VoladoraAparecio=1;
		}
	}
	
	public void desaparecePocionVoladoraEn(int tick) {
		if(pocionVoladoraEnEscenario!=null) {
			pocionVoladoraEnEscenario.setTime(tick);
			if(pocionVoladoraEnEscenario.getTime()==300) {
				pocionVoladoraEnEscenario=null;
			}
		}	
	}
	
	public void desaparecePocionVoladoraEnMulti(int tick) {
		if(pocionVoladoraEnEscenario!=null) {
			pocionVoladoraEnEscenario.setTime(tick);
			if(pocionVoladoraEnEscenario.getTime()==300) {
				pocionVoladoraEnEscenario=null;
			}
		}	
		if(pocion2VoladoraEnEscenario!=null) {
			pocion2VoladoraEnEscenario.setTime(tick);
			if(pocion2VoladoraEnEscenario.getTime()==300) {
				pocion2VoladoraEnEscenario=null;
			}
		}
	}
	
	//------PODER RAFAGA-----
	public void apareceItemPocionPoderControlado() {
		if(pocionPoderControladoAparecio==0) {// Si la pocion no se encuentra creada, la crea.
			pocionPoderControladoEnEscenario= new PocionPoderControlado(100, 550, 10, 10);
			pocionPoderControladoAparecio=1;
		}
	}
	public void apareceItemPocionPoderControladoMulti() {
		if (pocionPoderControladoAparecio==0 && pocion2PoderControladoAparecio==0) {
			//POCION 1
			pocionPoderControladoEnEscenario= new PocionPoderControlado(100,550,10,10);
			pocionPoderControladoAparecio=1;
			//POCION 2
			pocion2PoderControladoEnEscenario= new PocionPoderControlado(510,550,10,10);
			pocion2PoderControladoAparecio=1;
		}
	}
	
	public void desaparecePocionPoderControlado(int tick) {
		if(pocionPoderControladoEnEscenario!=null) {
			pocionPoderControladoEnEscenario.setTime(tick);
			if(pocionPoderControladoEnEscenario.getTime()==300) {
				pocionPoderControladoEnEscenario=null;
			}
		}	
	}
	
	public void desaparecePocionPoderControladoMulti(int tick) {
		if(pocionPoderControladoEnEscenario!=null) {
			pocionPoderControladoEnEscenario.setTime(tick);
			if(pocionPoderControladoEnEscenario.getTime()==300) {
				pocionPoderControladoEnEscenario=null;
			}
		}	
		if(pocion2PoderControladoEnEscenario!=null) {
			pocion2PoderControladoEnEscenario.setTime(tick);
			if(pocion2PoderControladoEnEscenario.getTime()==300) {
				pocion2PoderControladoEnEscenario=null;
			}
		}	
	}
	
	//-----BARRA DE INICIO---
	public Viga barraInicio() {
		return 	barra1;//LA BARRA 1 ES LA BARRA DEL INICIO
	}
	//---- MUSICA FONDO---
	public void play(int x) {//Comienza a reproducir la musica que le corresponde a cada escenario.
		if (x==0){
			musica0.loop(1000);
		}
		if (x==1){
			musica1.loop(1000);
		}
		if (x==2){
			musica2.loop(1000);
		}
	}
	public void stop(int x) {//Pausa la cancion que se le pasa como parametro.
		if (x==0){
			musica0.stop();
		}
		if (x==1){
			musica1.stop();
		}
		if (x==2){
			musica2.stop();
		}
	}
	public int getNumeroEscenario() {
		return numeroEscenario;
	}

	
}	
