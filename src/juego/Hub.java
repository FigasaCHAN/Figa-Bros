package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;


public class Hub {

	private Viga corazon1,corazon2,corazon3,corazon4,corazon5,corazon6;
	private int hubMago;
	private static Image life= Herramientas.cargarImagen("life.gif");
	
	public Hub(int hub) {
		hubMago=hub;
		//Se crean los corazones.
		if (hub==1) {  //Mago1
			this.corazon1= new Viga(60,35,20,20,life);
			this.corazon2= new Viga(90,35,20,20,life);
			this.corazon3= new Viga(120,35,20,20,life);
		}
		if (hub==2) {  //Mago 2
			this.corazon4 = new Viga(690,35,20,20,life);
			this.corazon5 = new Viga(720,35,20,20,life);
			this.corazon6 = new Viga(750,35,20,20,life);
		}
	}
	
	public void dibujarVidas(Entorno entorno,Mago mago) {
		//Se evalua la cantidad de vidas y se dibuja en base de cuantas vidas tiene el player.
		if (hubMago==1) { //Si el hubMago es 1 solo hay un mago
			if (mago.cantidadDeVidas()==3) {
				corazon1.dibujarVigaConImagen(entorno);
				corazon2.dibujarVigaConImagen(entorno);
				corazon3.dibujarVigaConImagen(entorno);
			}
			if (mago.cantidadDeVidas()==2) {
				corazon1.dibujarVigaConImagen(entorno);
				corazon2.dibujarVigaConImagen(entorno);
			}
			if (mago.cantidadDeVidas()==1){
				corazon1.dibujarVigaConImagen(entorno);
			}
		}else { //Si hubMago no es 1 significa que se necesita dibujar el hub del otro mago
			if (mago.cantidadDeVidas()==3) {
				corazon4.dibujarVigaConImagen(entorno);
				corazon5.dibujarVigaConImagen(entorno);
				corazon6.dibujarVigaConImagen(entorno);
			}
			if (mago.cantidadDeVidas()==2) {
				corazon4.dibujarVigaConImagen(entorno);
				corazon5.dibujarVigaConImagen(entorno);
			}
			if (mago.cantidadDeVidas()==1){
				corazon4.dibujarVigaConImagen(entorno);
			}
		}
	}
	public void dibujarPuntos(Entorno entorno, Mago mago) {
		//Aca se pasa de String a int ("valueOf" es una propiedad para quitarle el valor int y pasarlo a String).
		String puntos = String.valueOf(mago.cantidadDePuntos());
		entorno.cambiarFont("Consolas", 20, Color.RED);
		entorno.escribirTexto("Puntos:", 45, 80);
		entorno.escribirTexto(puntos, 125, 80);
	}
}
