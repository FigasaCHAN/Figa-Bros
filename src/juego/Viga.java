package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Viga {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private int angulo;
	private Color color;
	private Image imagen;
	
	public Viga(double x, double y, double alto, double ancho, Color color) {
		this.x= x;
		this.y= y;
		this.alto= alto;
		this.ancho= ancho;
		this.angulo=0;
		this.color= color;
	}
	
	public Viga(double x, double y, double alto, double ancho, Image imagen) {
		this.x= x;
		this.y= y;
		this.alto= alto;
		this.ancho= ancho;
		this.angulo= 0;
		this.imagen= imagen;
	}
	
	//Dibujar vigas sin imagenes.
	public void dibujarViga(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, ancho, alto, angulo, color);
	}
	
	//Dibujar vigas con imagenes.
	public void dibujarVigaConImagen(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, angulo);
	}
	
	//Set y Get.
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public void setColor(Color color) {
		this.color=color;
	}

}
