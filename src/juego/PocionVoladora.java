package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class PocionVoladora {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private boolean activado;
	private int time; //PARA SABER CUANTO TIEMPO ESTUVO
	private static Image imagen= Herramientas.cargarImagen("pocionVoladora.gif");
	private int angulo;
	private double escala;
	
	public PocionVoladora(){
		activado=false;
	}

	public PocionVoladora(double posicionX, double posicionY, double ancho, double alto){ //Este contructor se usa en el escenario
		this.x= posicionX;
		this.y= posicionY;
		this.ancho= ancho;
		this.alto= alto;
		this.angulo=0;
		this.escala=0.5;
		this.activado=false;
		this.time=0;//UNA VEZ QUE APARECE, EL TIEMPO ES CERO
	}
	public void dibujarPocionVoladora(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, angulo, escala);
	}

	//Get y Set.
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public boolean getActivado() {
		return activado;
	}
	public void setActivado(boolean activarOdesactivar) {
		activado=activarOdesactivar;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int tick) {//Suma un numero, solo suma un tick.
		time+=tick;
	}
}