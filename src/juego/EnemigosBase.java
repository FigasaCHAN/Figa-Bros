package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class EnemigosBase {
	
	private double x,auxiliarX;
	private double y,auxiliarY;
	private double alto;
	private double ancho;
	private double angulo;
	private int congelado;
	private int rodar;  //Si es 0 entonces no esta rodando, si es 1 entonces si.
	
	//IMAGENES 
	private  Image imagen =Herramientas.cargarImagen("enemigo.png");//IMAGEN POR DEFECTO
	private static Image imagenDerecha=Herramientas.cargarImagen("enemigoDerecha.gif");
	private static Image imagenIzquierda=Herramientas.cargarImagen("enemigoIzquierda.gif");
	private static Image congeladoDerecha=Herramientas.cargarImagen("enemigoDerechaCongelado.png");
	private static Image congeladoIzquierda=Herramientas.cargarImagen("enemigoIzquierdaCongelado.png");
	
	private int direccionEnemigo; //0 si se esta moviendo para la izquierda // 1 si es derecha
	private int direccionDeRodar;
	private int patron; //indica las veces que toca los bordes de la viga en la que esta parado
	
	public EnemigosBase(double x, double y, double alto, double ancho) {
		this.x=x;
		this.auxiliarX=x;
		this.auxiliarY=y;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;
		this.angulo=0;
		this.direccionDeRodar=0;
		this.congelado=0;
		this.rodar=0;
	}
	
	//---------FUNCIONES BASICAS---------
	public void dibujarEnemigo(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, angulo);
	}

	public void moverseIzquierda() {
		imagen = imagenIzquierda;
		x=x-1;
		
	}
	public void moverseDerecha() {
		imagen = imagenDerecha;
		x=x+1;
	}
	public void moverseIzquierdaMulti() {
		imagen = imagenIzquierda;
		x=x-0.5;

	}
	public void moverseDerechaMulti() {
		imagen = imagenDerecha;
		x=x+0.5;
	}
	
	public void caer(){
		y=y+1;
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public int getDireccion() {
		return direccionEnemigo;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public boolean llegasteAlFinal(Viga viga){
		return y+alto/2>=viga.getY()-viga.getAlto()/2;
	}
	public void volverAlInicio() {
		x=auxiliarX;
		y=auxiliarY;
	}

	public boolean agujero(Viga viga) {
		if ((y+alto/2>=viga.getY()-viga.getAlto()/2) && (y+alto/2<=viga.getY()+viga.getAlto()/2) && (viga.getX()-viga.getAncho()/2<= x+ancho/2 && viga.getX()+viga.getAncho()/2>=x-ancho/2)){
			return false; //devuelve false cuando no hay agujero, es decir, que el objeto choca con una viga
		}
		return true; //devuelve true cuando hay agujero
	}
	
	//------LIMITES DE MOVIMIENTO-----
	public boolean chocoConDerecha(Viga derecha) {
		return x+ancho/2>=derecha.getX()-derecha.getAncho()/2;
	}
	public boolean chocoConIzquierda(Viga izquierda) {
		return x-ancho/2<=izquierda.getX()+izquierda.getAncho()/2;
	}
	
	public void cambiarDireccion(Viga derecha, Viga izquierda) { //ahora le paso directamenta, las vigas que necesito... que son las BARRAS IZQUIERDA/DERECHA
		if (chocoConDerecha(derecha) ) {
			moverseIzquierda();
			direccionEnemigo=0;//indica que se esta moviendo a la izquierda
		}
		if (chocoConIzquierda(izquierda) ) {
			moverseDerecha();
			direccionEnemigo=1; //indica que se esta moviendo a la derecha
		}
	}

	//---------MOVERSE------

	public void moverse(Viga derecha, Viga izquierda, Viga parado) { //le paso viga parado para que se mueva cierta cantidad de veces sobre esa viga
		if (!chocoConIzquierda(izquierda) && !chocoConDerecha(derecha) && direccionEnemigo==0) { //si no choca con nada y cont=0(significa que se mueve hacia la izquierda)
			moverseIzquierda();
		}
		if (!chocoConIzquierda(izquierda) && !chocoConDerecha(derecha) && direccionEnemigo==1) { //si no choca con nada y cont=1(significa que se mueve hacia la derecha)
			moverseDerecha();
		}
		if(parado != null && puntoIzquierdoDeViga(parado) && patron>=0 && patron<2) {

			direccionEnemigo=1; //cambio la direccion
			patron++; //sumo patron, porque toco el borde izquierdo

		}
		//parado va valer null cuando esta sobre las vigas del suelo o si el enemigo esta rodando
		if(parado != null && puntoDerechoDeViga(parado) && patron>=0 && patron<2 ) { 
			
			direccionEnemigo=0;//cambio la direccion
			patron++; //sumo patron porque toco el borde derecho 	
			
		}
		if (chocoConIzquierda(izquierda)) {
			cambiarDireccion(derecha, izquierda);// si choca, simplemente cambia de direcci�n 
		}
		if (chocoConDerecha(derecha)) {
			cambiarDireccion(derecha, izquierda);
		}
	}
	public void moverseMulti(Viga derecha, Viga izquierda, Viga parado) { //le paso viga parado para que se mueva cierta cantidad de veces sobre esa viga
		if (!chocoConIzquierda(izquierda) && !chocoConDerecha(derecha) && direccionEnemigo==0) { //si no choca con nada y cont=0(significa que se mueve hacia la izquierda)
			moverseIzquierdaMulti();
		}
		if (!chocoConIzquierda(izquierda) && !chocoConDerecha(derecha) && direccionEnemigo==1) { //si no choca con nada y cont=1(significa que se mueve hacia la derecha)
			moverseDerechaMulti();
		}
		if(parado != null && puntoIzquierdoDeViga(parado) && patron>=0 && patron<2) {

			direccionEnemigo=1; //cambio la direccion
			patron++; //sumo patron, porque toco el borde izquierdo

		}
		//parado va valer null cuando esta sobre las vigas del suelo o si el enemigo esta rodando
		if(parado != null && puntoDerechoDeViga(parado) && patron>=0 && patron<2 ) { 

			direccionEnemigo=0;//cambio la direccion
			patron++; //sumo patron porque toco el borde derecho 

		}

		if (chocoConIzquierda(izquierda)) {
			cambiarDireccion(derecha, izquierda);// si choca, simplemente cambia de direcci�n 
		}
		if (chocoConDerecha(derecha)) {
			cambiarDireccion(derecha, izquierda);
		}
	}
	//-----PATRON DE MOVIMIENTO-------
	public boolean puntoIzquierdoDeViga(Viga viga) { 
		return x-ancho/2 <= viga.getX()-viga.getAncho()/2;
		//Devuelve true cuando el enemigo toca el final(parte izquierda) de la viga
	}
	public boolean puntoDerechoDeViga(Viga viga) {  
		return x+ancho/2 >= viga.getX()+viga.getAncho()/2; 
		//Devuelve true cuando el enemigo toca el final(parte derecha) de la viga
	}
	public int getPatron() {
		return patron;
	}
	public void setPatron(int x) {
		patron=x;
	}

	//------Congelamiento------
	public void estasCongelado() {
		if(direccionEnemigo==0) { //si el enemigo se esta moviendo a la izquierda
			imagen= congeladoIzquierda;
		}
		if(direccionEnemigo==1) { //si el enemigo se esta moviendo a la derecha
			imagen= congeladoDerecha;
		}
		congelado++;  //El contador avanza y por ende significa que el enemigo esta congelado.
	}
	public void descongelate() {
		congelado=0;  //Vuelve el contador a 0 entonces el enemigo se puede volver a mover.
	}
	public int getCongelado() {
		return congelado;  //Retorna el tiempo que esta congelado.
	}
	//----------RODAR---------------
	public void rodar() {
		rodar=1;
	}
	public int getRodar() {
		return rodar;
	}
	public void setDireccionRodar(int x) {
		direccionDeRodar=x;
	}
	public int getDireccionRodar() {
		return direccionDeRodar;
	}
	
	//REALIZA LOS "PATRONES" DE MOVIMIENTO DE LOS ENEMIGOS QUE ESTAN RODANDO.
	public void empujados(Viga derecha, Viga izquierda) { 
		//Dependiendo hacia donde este mirando el mago, el enemigo congelado comenzara a rodar hacia esa misma direccion.
		if (!chocoConDerecha(derecha) && direccionDeRodar==1) {
			x=x+5/2;
		}
		if (!chocoConIzquierda(izquierda)&& direccionDeRodar==0) {
			x=x-5/2;
		}
		//Realiza el cambio de direccion.
		if (chocoConDerecha(derecha)&&direccionDeRodar==1){
			direccionDeRodar=0;
		}
		if (chocoConIzquierda(izquierda)&&direccionDeRodar==0){
			direccionDeRodar=1;
		}
	}
	
	//-----IMPACTOS-------------
	
	public boolean chocasteCon(Mago mago) { //CHOCA CON MAGO
		return x-ancho/2<=mago.getX()+mago.getAncho()/2 && y-alto/2<=mago.getY()+mago.getAlto()/2
				&& x+ancho/2>=mago.getX()-mago.getAncho()/2 && y+alto/2>=mago.getY()-mago.getAlto()/2;
	}
	
	public boolean chocasteCon(EnemigosBase enemigo) {//CHOCA CON ENEMIGOS
		if (enemigo.getRodar()==0) {
			return x-ancho/2<=enemigo.getX()+enemigo.getAncho()/2 && y-alto/2<=enemigo.getY()+enemigo.getAlto()/2
					&& x+ancho/2>=enemigo.getX()-enemigo.getAncho()/2 && y+alto/2>=enemigo.getY()-enemigo.getAlto()/2;
		}else {
			return false;
		}
	}
	
	public boolean fuisteHeridoCon(PoderCongelante poder) { //ES GOLPEADO
		return x-ancho/2<=poder.getX()+poder.getAncho()/2 && y-alto/2<=poder.getY()+poder.getAlto()/2
				&&x+ancho/2>=poder.getX()-poder.getAncho()/2 && y+alto/2>=poder.getY()-poder.getAlto()/2;
	}
	
}