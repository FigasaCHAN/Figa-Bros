# Figa Bros

En el marco de la materia programación I, se nos pidió realizar un juego “La torre de Magos”. El juego posee un mago, enemigos y vigas horizontales.
El mago debe desplazarse por las vigas, cada vez que el mago caiga a una viga, lanza un hechizo congelante, si este hechizo impacta con un enemigo, el enemigo se congela por un tiempo. Si el hechizo impacta con los enemigos o impacta con la pared, el hechizo desaparece.
Si el mago toca al enemigo congelado, el enemigo es empujado y se desplaza por las vigas, si este enemigo llega a chocar con un enemigo no congelado, el enemigo no congelado es arrastrado por el enemigo congelado.
Si el mago toca a un enemigo no congelado, el mago muere de inmediato.
El enemigo congelado o arrastrado llega al final del escenario, el enemigo es eliminado. Si llegan al final del mapa y no están congelado ni están siendo arrastrados, el enemigo vuelve a aparecer en la viga que apareció por primera vez.
